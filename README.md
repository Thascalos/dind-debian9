# Docker in docker - Debian 9

Simple implementation of dind in debian 9 base image. For testing purposes it also contains another script for test build image with "hello-world" flask app. This app also runs with strace, again, for testing purposes.

## Usage

### Build a Docker image

```shell
$ docker build -t dind-debian9:v1.0 .
```

### Run it in Docker

```shell
$ docker run --privileged -it dind-debian9:v1.0 /build_image_and_run_w_strace.sh
```
