FROM debian:9

RUN apt-get update && apt-get install -y --no-install-recommends  \
  apt-transport-https \
  ca-certificates \
  curl \
  gnupg2 \
  software-properties-common \
  git \
  strace

RUN curl -fsSL https://download.docker.com/linux/debian/gpg | apt-key add -
RUN add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/debian \
   $(lsb_release -cs) \
   stable"
RUN apt-get update && apt-get install -y --no-install-recommends docker-ce && \
  docker -v && \
  dockerd -v

# DIND Script
ENV DOCKER_EXTRA_OPTS '--storage-driver=overlay'
RUN curl -fL -o /usr/local/bin/dind "https://raw.githubusercontent.com/moby/moby/master/hack/dind" && \
	chmod +x /usr/local/bin/dind

COPY entrypoint.sh /usr/local/bin/
RUN chmod +x /usr/local/bin/entrypoint.sh

COPY build_image_and_run_w_strace.sh /build_image_and_run_w_strace.sh
RUN chmod +x /build_image_and_run_w_strace.sh

VOLUME /var/lib/docker
EXPOSE 2375
ENTRYPOINT ["/usr/local/bin/entrypoint.sh"] 